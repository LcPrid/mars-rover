import React from 'react';
import Alert from 'react-bootstrap/lib/Alert';

const Messages = ({alert}) => {
  return (
    <Alert bsStyle={alert.type}>
    {alert.message}
  </Alert>
  );
}

export default Messages;