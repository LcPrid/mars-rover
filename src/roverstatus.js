import React from 'react';
import Panel from 'react-bootstrap/lib/Panel';


const RoverStatus = ({rover}) => {
  return (
    <div className="column">
    <Panel header="Status" bsStyle="danger">
      <p>Facing: {rover.facing}</p>
      <p>Location: {rover.location}</p>
      </Panel>
     </div>
  );
};

export default RoverStatus;