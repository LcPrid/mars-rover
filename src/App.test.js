import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import TestUtils from 'react-addons-test-utils'

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<App />, div);
});

it('calculates location correctly when facing East', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.row = 1;
thisRover.column = 5;
thisRover.location = 50;
thisRover.facing = 'East';
view.setState({rover: thisRover});
view.moveLocation(10)
expect(view.state.rover.location).toBe(60);
})

it('calculates location correctly when facing South', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.row = 1
thisRover.column = 7
thisRover.location = 71;
thisRover.facing = 'South';
view.setState({rover: thisRover});
view.moveLocation(15)
expect(view.state.rover.location).toBe(1571)
})

it('calculates location correctly when facing North', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.row = 20;
thisRover.column = 6;
thisRover.location = 2006;
thisRover.facing = 'North';
view.setState({rover: thisRover});
view.moveLocation(5)
expect(view.state.rover.location).toBe(1506)
})

it('calculates location correctly when facing West', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.row = 5;
thisRover.column = 67;
thisRover.location = 567;
thisRover.facing = 'West';
view.setState({rover: thisRover});
view.moveLocation(50)
expect(view.state.rover.location).toBe(517)
})

it('changes direction correctly when facing East - Right', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'East';
view.setState({rover: thisRover});
view.changeDirection('right')
expect(view.state.rover.facing).toBe('South')

})

it('changes direction correctly when facing East - Left', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'East';
view.setState({rover: thisRover});
view.changeDirection('left')
expect(view.state.rover.facing).toBe('North')

})

it('changes direction correctly when facing South - Right', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'South';
view.setState({rover: thisRover});
view.changeDirection('right')
expect(view.state.rover.facing).toBe('West')

})

it('changes direction correctly when facing South - Left', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'South';
view.setState({rover: thisRover});
view.changeDirection('left')
expect(view.state.rover.facing).toBe('East')

})

it('changes direction correctly when facing West - Right', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'West';
view.setState({rover: thisRover});
view.changeDirection('right')
expect(view.state.rover.facing).toBe('North')

})

it('changes direction correctly when facing West - Left', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'West';
view.setState({rover: thisRover});
view.changeDirection('left')
expect(view.state.rover.facing).toBe('South')

})

it('changes direction correctly when facing North - Right', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'North';
view.setState({rover: thisRover});
view.changeDirection('right')
expect(view.state.rover.facing).toBe('East')

})

it('changes direction correctly when facing North - Left', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.facing = 'North';
view.setState({rover: thisRover});
view.changeDirection('left')
expect(view.state.rover.facing).toBe('West')

})

it('calculates location correctly with multiple commands', () => {
var view = TestUtils.renderIntoDocument(<App />)
var commands = [{command: 'distance', value: '50', text: 'commandtext', id: 0},
{command: 'direction', value: 'left', text: 'commandtext', id: 1},
{command: 'distance', value: '23', text: 'commandtext', id: 2},
{command: 'direction', value: 'left', text: 'commandtext', id: 3},
{command: 'distance', value: '4', text: 'commandtext', id: 4}]
view.setState({data: commands});
commands.map((command) => {view.runCommand(command.command, command.value);});
expect(view.state.rover.location).toBe(4624);
expect(view.state.rover.facing).toBe('North')
})

it('calculates location correctly with multiple commands 2', () => {
var view = TestUtils.renderIntoDocument(<App />)
var commands = [{command: 'distance', value: '10', text: 'commandtext', id: 0},
{command: 'direction', value: 'left', text: 'commandtext', id: 1},
{command: 'distance', value: '10', text: 'commandtext', id: 2},
{command: 'direction', value: 'left', text: 'commandtext', id: 3},
{command: 'distance', value: '5', text: 'commandtext', id: 4}]
view.setState({data: commands});
commands.map((command) => {view.runCommand(command.command, command.value);});
expect(view.state.rover.location).toBe(511);
expect(view.state.rover.facing).toBe('North');
})

it('calculates location correctly with multiple commands 3', () => {
var view = TestUtils.renderIntoDocument(<App />)
var commands = [{command: 'direction', value: 'left', text: 'commandtext', id: 0},
{command: 'distance', value: '55', text: 'commandtext', id: 1},
{command: 'direction', value: 'right', text: 'commandtext', id: 2},
{command: 'distance', value: '10', text: 'commandtext', id: 3},
{command: 'direction', value: 'right', text: 'commandtext', id: 4}]
view.setState({data: commands});
commands.map((command) => {view.runCommand(command.command, command.value);});
expect(view.state.rover.location).toBe(1056);
expect(view.state.rover.facing).toBe('West');
})


it('allows no more than 5 commands', () => {
var view = TestUtils.renderIntoDocument(<App />)
var alert = {message: 'you can only enter a maximum of 5 commands', type: 'warning', time: 2000}
var commands = [{command: 'commandtype', value: 'commandvalue', text: 'commandtext', id: 0},
{command: 'commandtype', value: 'commandvalue', text: 'commandtext', id: 1},
{command: 'commandtype', value: 'commandvalue', text: 'commandtext', id: 2},
{command: 'commandtype', value: 'commandvalue', text: 'commandtext', id: 3},
{command: 'commandtype', value: 'commandvalue', text: 'commandtext', id: 4}]
view.setState({data: commands});
var command = {command: 'commandtype', value: 'commandvalue', text: 'commandtext', id: 5}
view.addCommand(command)
expect(view.state.data.length).toBe(5)
expect(view.state.alert.message).toBe(alert.message)
expect(view.state.alert.type).toBe(alert.type)
})

it('halts commands when at the perimeter when facing North', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.row = 2;
thisRover.location = 150;
thisRover.facing = 'North';
view.setState({rover: thisRover});
view.moveLocation(3)
expect(view.state.rover.location).toBe(50);
})

it('halts commands when at the perimeter when facing East', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.column = 97;
thisRover.location = 597;
thisRover.facing = 'East';
view.setState({rover: thisRover});
view.moveLocation(6)
expect(view.state.rover.location).toBe(600);
})
it('halts commands when at the perimeter when facing South', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.row = 99;
thisRover.location = 99900;
thisRover.facing = 'South';
view.setState({rover: thisRover});
view.moveLocation(3)
expect(view.state.rover.location).toBe(100000);
})

it('halts commands when at the perimeter when facing West', () => {
var view = TestUtils.renderIntoDocument(<App />)
var thisRover = view.state.rover;
thisRover.column = 7;
thisRover.location = 5007;
thisRover.facing = 'West';
view.setState({rover: thisRover});
view.moveLocation(10)
expect(view.state.rover.location).toBe(5001);
})