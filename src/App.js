import React, { Component } from 'react';
import logo from './images/logo.png';
import './App.css';
import Title from './title';
import CommandForm from './commandForm';
import CommandList from './commandList';
import RoverStatus from './roverstatus';
import Messages from './messages';

window.id = 0;
class App extends Component {
  constructor(props){
    // Pass props to parent class
    super(props);
    this.setAlert = this.setAlert.bind(this);
    // Set initial state
    this.state = {
      data: [],
      rover: {
        facing: "South",
        location: 1,
        column: 1,
        row: 1,
        maxColumn: 100,
        minColumn: 1,
        maxRow: 100,
        minRow: 1
      },
      alert: {
      message: '',
      type: '',
      time: 0
    }
    }
  
  };

  setAlert(alertType, alertMessage){
    const thisAlert = {message: alertMessage, type: alertType, time: 2000}
    this.setState({alert: thisAlert});
  };

   // Add command handler
  addCommand(commandtype, commandvalue, commandtext){
    // Assemble data
    const command = {command: commandtype, value: commandvalue, text: commandtext, id: window.id++}
    //
    if (this.state.data.length < 5){
    // Update data
    this.state.data.push(command);
    // Update state
    this.setState({data: this.state.data});
    }
    else{

    this.setAlert('warning', 'you can only enter a maximum of 5 commands');
   
    }
  };
 
  clearCommands(){
    this.setState({data: []});
  };

  runCommand(commandtype, commandvalue){
    switch(commandtype){
      case "direction":
        this.changeDirection(commandvalue);
        this.setState({rover: this.state.rover})
        break;
      case "distance":
        var distance = Number(commandvalue);
        this.moveLocation(distance);
        this.setState({rover: this.state.rover})
        break;
      default:
      break;
    }
  };

  changeDirection(direction){
    var thisRover = this.state.rover;
    if (direction === "left"){
    switch(thisRover.facing) {
    case "North":
        thisRover.facing = "West"
        this.setState({rover: thisRover});
        break;
    case "East":
        thisRover.facing = "North"
        this.setState({rover: thisRover});
        break;
    case "South":
        thisRover.facing = "East"
        this.setState({rover: thisRover});
        break;
    case "West":
        thisRover.facing = "South"
        this.setState({rover: thisRover});
        break;
    default:
    break;
    }}
    else if (direction === "right")
    {
    switch(thisRover.facing) {
    case "North":
        thisRover.facing = "East"
        this.setState({rover: thisRover});
        break;
    case "East":
        thisRover.facing = "South"
        this.setState({rover: thisRover});
        break;
    case "South":
        thisRover.facing = "West"
        this.setState({rover: thisRover});
        break;
    case "West":
        thisRover.facing = "North"
        this.setState({rover: thisRover});
        break;
    default:
      break;
    } 
    }
       
  };

  moveLocation(distance){
    var thisRover = this.state.rover;
    switch(thisRover.facing) {
    case "North":
        if ((thisRover.row - distance) >= thisRover.minRow){
        thisRover.location -= (distance * 100);
        thisRover.row -= distance;
        this.setState({rover: thisRover});
        break;
        }
        else
        {
          var newDistance = thisRover.row - thisRover.minRow
          thisRover.location -= (newDistance * 100);
          thisRover.row -= newDistance;
          this.setState({rover: thisRover});
          break;
          }
      case "East":
      if ((thisRover.column + distance) <= thisRover.maxColumn){
        thisRover.location += distance;
        thisRover.column += distance;
        this.setState({rover: thisRover});
        break;
        }
        else{
          newDistance = thisRover.maxRow - thisRover.column
          thisRover.location += newDistance;
          thisRover.column += newDistance;
          this.setState({rover: thisRover});
          break;
        }
    case "South":
        if ((thisRover.row + distance) <= thisRover.maxRow){
        thisRover.location += (distance * 100);
        thisRover.row += distance;
        this.setState({rover: thisRover});
        break;
        }
        else
        {
          newDistance = thisRover.maxRow - thisRover.row
          thisRover.location += (newDistance * 100);
          thisRover.row += newDistance;
          this.setState({rover: thisRover});
          break;
          }
    case "West":
        if ((thisRover.column - distance) >= thisRover.minColumn){
        thisRover.location -= distance;
        thisRover.column -= distance;
        this.setState({rover: thisRover});
        break;
        }
        else{
          newDistance = thisRover.column - thisRover.minRow;
          thisRover.location -= newDistance;
          thisRover.column -= newDistance;
          this.setState({rover: thisRover});
          break;
        }
      default:
      break;
    }};
    

  
  render() {
    return (
      <div className="App">
        <link rel='stylesheet' type="text/css" href='https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css' />
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <Title/>
        </div>
        <div>
          <div>
            <CommandForm addCommand={this.addCommand.bind(this)} />
            <CommandList runCommand={this.runCommand.bind(this)} commands={this.state.data} clearCommands={this.clearCommands.bind(this)} />
            <RoverStatus rover={this.state.rover}/>
          </div>
        </div>
      </div>
      
    );
  }
}

export default App;
