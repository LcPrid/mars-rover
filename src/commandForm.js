import React from 'react';
import Button from 'react-bootstrap/lib/Button';
import ButtonToolbar from 'react-bootstrap/lib/ButtonToolbar';
import Panel from 'react-bootstrap/lib/Panel';

const CommandForm = ({addCommand}) => {
  // Input tracker
  let commandValue;
  let commandDescription;
  
  return (
    <div className='column' >
    <Panel header="Move Mars Rover" bsStyle="danger">
    <ButtonToolbar>
    <Button bsStyle="danger" onClick={() => {addCommand('direction', 'right', 'Turn Right');}}>
        Turn Right
      </Button>
    <Button bsStyle="danger" onClick={() => {addCommand('direction', 'left', 'Turn Left');}}>
        Turn Left
      </Button>
      </ButtonToolbar>
      <br/><br/>
      <div className="wrapper">
          <input ref={input => {commandValue = input;}} type="number" placeholder="Enter distance in metres" />
      <Button bsStyle="danger" onClick={() => {
       commandDescription = "Move " + commandValue.value + " metres"
       addCommand('distance', commandValue.value, commandDescription);
       commandValue.value = '';
    }}>
        Move
      </Button>  
      </div>
      
  </Panel>
    </div>
  );
};

export default CommandForm;