import React from 'react';
import Panel from 'react-bootstrap/lib/Panel';
import Button from 'react-bootstrap/lib/Button';
import ListGroup from 'react-bootstrap/lib/ListGroup'
import ListGroupItem from 'react-bootstrap/lib/ListGroupItem'

const Command = ({command}) => {
  // Each command
  return (<ListGroupItem>{command.text}</ListGroupItem>);
}

const CommandList = ({runCommand, commands, clearCommands}) => {
  // Map through the commands
  const commandNode = commands.map((command) => {
    return (<Command command={command} key={command.id}/>)
  });
  return (<div className="column">
  <Panel header="Command List" bsStyle="danger">
      <ListGroup>{commandNode}</ListGroup>
     <Button bsStyle='danger' disabled={commands.length === 0} onClick={() => {
    commands.map((command) => {
      runCommand(command.command, command.value);
      
    });
    clearCommands();
    }}>
        Run Commands
      </Button>
    </Panel>
  </div>

    );
}

export default CommandList;